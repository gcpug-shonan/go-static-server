# 概要

Goでstaticサーバーを書いて、Cloud Run で動かすのを試す。

Cloud Build でのBuildもやる。

## Goのdocker image作成

```
cd server
docker build  -t gcr.io/$PROJECT_ID/go-static-server .
```

## CloudBuild時のIAMの設定

こちらのcloud build用 service accountに権限を付ける。
[project_number]@cloudbuild.gserviceaccount.com


`Service Account User`  
`Cloud Run Admin`
